import { Component } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  private questionSubject = new BehaviorSubject(null);
  public question = this.questionSubject.asObservable();

  private statusSubject = new BehaviorSubject("waitingCards");
  public status = this.statusSubject.asObservable();

  constructor(){
    let that = this;
    let xhr = new XMLHttpRequest()
    xhr.open('GET', 'http://localhost:4040/getQuestion')
    xhr.send()
    xhr.onload = function() {
      if (xhr.status != 200) {
        alert(`Error ${xhr.status}: ${xhr.statusText}`)
      } else {
        let response = JSON.parse(xhr.response);
        that.questionSubject.next(response.text);
      }
    };
  }
}
