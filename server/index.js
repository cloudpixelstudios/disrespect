const express = require('express');
const app = express();
const port = 4040;
const admin = require('firebase-admin');
const cors = require('cors');
const uuidV4 = require('uuid/v4');
const randomize = require('randomatic');
const questions = require('./questions.json');
const answers = require('./answers.json');
var serviceAccount = require("./serviceKey.json");
app.use(cors());

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://disrespect-8cb93.firebaseio.com"
});

let db = admin.database();

app.get('/setupGame', (req, res) => {
  let uuid = uuidV4();
  let code = randomize("A0", "6", { exclude: '0oOiIlL1' });
  var gameRef = db.ref('lobby/' + uuid);
  let game = {
    "game_id": uuid,
    "join_code": code,
    "players":[],
    "lobby_time": Math.floor(new Date().getTime()/1000.0)
  }
  gameRef.set(game);
  res.send(JSON.stringify(game));
})

app.get('/startGame', (req, res) => {
  if (req.query.gameUUID && req.query.gameCode) {
    let uuid = req.query.gameUUID;
    let code = req.query.gameCode;
    var gameRef = db.ref('lobby/' + uuid);
    gameRef.once("value", function(snapshot) {
      let game = snapshot.val();
      let playersWithCards = [];
      for(let i = 0; i < game.players.length; i++) {
        let cards = getNewAnswerDeck();
        let temp = game.players[i];
        temp["cards"] = cards;
        playersWithCards.push(temp);
      }
      let gameUpdate = {
        "start_time": Math.floor(new Date().getTime()/1000.0),
        "players": playersWithCards
      }
      gameRef.update(gameUpdate);
      res.send("Cards Updated");
    });
  } else {
    res.send("Error");
  }
})

function getNewAnswerDeck(playersCount) {
  let randomAnswers = [];
  while(randomAnswers.length < 7){
      var r = Math.floor(Math.random() * 7);
      if(randomAnswers.indexOf(r) === -1) randomAnswers.push(r);
  }
  let cardAnswers = [];
  for(let i = 0; i < randomAnswers.length; i++) {
    cardAnswers.push(answers.answers[randomAnswers[i]])
  }
  return cardAnswers;
}

app.get('/getQuestion', (req, res) => {
  res.send(getQuestion());
})

function getQuestion() {
  let r = Math.floor(Math.random() * questions.questions.length);
  let question = questions.questions[r];
  return question;
}

app.get('/addPlayer', (req, res) => {
  if (req.query.gameUUID) {
    let playerInfo = req.query.playerInfo;
    let uuid = req.query.gameUUID;
    var gameRef = db.ref('lobby/' + uuid);
    gameRef.once("value", function(snapshot) {
      let game = snapshot.val();
      let players = [];
      if(game.players) {
        players = game.players;
      }
      players.push({"first_name": "Keegan", "last_name": "Cruickshank", "nickname": "Keg"});
      gameRef.update({"players": players})
      res.send("Player Added");
    }, function (errorObject) {
      console.log("The read failed: " + errorObject.code);
    });
  } else {
    res.send("Error");
  }
})

app.listen(port, () => {
  console.log(`Game service running on port ${port}!`)
})
